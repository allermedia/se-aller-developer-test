# Aller Media Sverige -- Developer test #

Hello and welcome to the Aller Media developer test.

### Background ###

We are currently mainly working with Wordpress, NodeJS, Laravel and React.  
This test IS NOT created for you to prove your excellence in any of the techniques listed above, it's more a way for us to get a feeling about how you approach guidelines and yourself as a developer.  
We expect you to already know some or learn by doing.

### Task. ###

* In NodeJS, create an API using ExpressJS with an endpoint `/articles` following the API documentation below.
* Create a Wordpress plugin with a widget that directly fetches from the NodeJS API and list the articles.

### API documentation ###

Fetch all articles

* __URL__

    GET /articles

* __Success response__

      Status: 200

        [
		  {
            "id": "586678522fe37e66355fcbd0",
            "title": "Article title 1",
            "content": "Lorem ipsum"
          },
		  {
		    "id": "586678522fe37e66355fcbd2",
            "title": "Article title 2",
            "content": "Ipsum lorem"
		  }
		]


### Guidelines ###

* Do NOT stress while coding your solution! Take your time and do it as good as possible.
* The PHP should follow the PSR2 code guidelines. http://www.php-fig.org/psr/psr-2/
* The Javascript should follow the Airbnb code guidelines. https://github.com/airbnb/javascript/
* Structure your code files in a tidy manner.
* In your PHP there should be as little html as possible, preferrably none, use templates.
* Document your code in English and create a README.md for both the API and the Wordpress plugin.
* Use git as VCS. You should commit often so we can see your progress.
* Use any kind of tools possible to ease your work. Examples: Docker, linters, npm, composer or code sniffers and include all possible settings and structures in the repo.

### Getting started ###

* Clone this repository, it contains some small startup helpers.

### Submission ###

* Compress this repository folder when done and send it to your contact person at Aller by email OR create your own public repository on Bitbucket/Github or any other provider and send us the link.  
* Explain to us what you had to learn to create your submission in the email.
* Please tell us how to improve this test.

### Documentation ###

* https://codex.wordpress.org/
* https://nodejs.org/en/docs/
* https://reactjs.org/docs/hello-world.html
* https://git-scm.com/documentation

__GOOD LUCK!__
